﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Selenium_Framework.Pages;
using System.Configuration;
using System.IO;

namespace Selenium_Framework.Tests
{
    [TestFixture]
    public class HomeTest
    {
        public HomePage Home;
        public IWebDriver driver;
        private string baseURL = ConfigurationManager.AppSettings["url"];

        [SetUp]
        public void Initialize()
        {
            driver = new ChromeDriver();
            Home = new HomePage(driver);
        }

        [Test]
        public void Login()
        {
            driver.Url = baseURL;
        }

        [TearDown]
        public void EndTest()
        {
            driver.Quit();
        }
    }
}
