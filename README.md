Following are the instructions of what needs to be automated:
1. Click on "Contact us" on the Home page.
2. Verify that user is redirected to the Contact Us page.
3. Select the "Subject Heading".
4. Enter Email address, Order reference and Message.
5. Click on "Send" button.
6. Verify that the message has been sent successfully.
